// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "FPCoreTypes.generated.h"

/**
 * 
 */
DECLARE_MULTICAST_DELEGATE(FOnDeathSignature);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnHealthChangedSignature, float, float);

UCLASS()
class FADEDPHOTO_API UFPCoreTypes : public UObject
{
	GENERATED_BODY()
	
};
