// Copyright Epic Games, Inc. All Rights Reserved.

#include "FadedPhotoGameMode.h"
#include "FadedPhotoCharacter.h"

AFadedPhotoGameMode::AFadedPhotoGameMode()
{
	// Set default pawn class to our character
	DefaultPawnClass = AFadedPhotoCharacter::StaticClass();	
}
